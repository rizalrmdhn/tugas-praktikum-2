package com.rizalramadhan_10181053.implementasifragment.adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.rizalramadhan_10181053.implementasifragment.fragment.HomeFragment;
import com.rizalramadhan_10181053.implementasifragment.fragment.StatusFragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    private int tabsCount;

    public FragmentAdapter(Context context, int tabCount, FragmentManager fragmentManager) {
        super(fragmentManager);

        this.context = context;
        this.tabsCount = tabCount;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                StatusFragment statusFragment = new StatusFragment();
                return statusFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabsCount;
    }
}
