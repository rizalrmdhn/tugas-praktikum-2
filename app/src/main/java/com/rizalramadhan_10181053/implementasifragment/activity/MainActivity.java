package com.rizalramadhan_10181053.implementasifragment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.rizalramadhan_10181053.implementasifragment.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnintent = findViewById(R.id.btnLogin);

        btnintent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contentintent = new Intent(MainActivity.this, ContentActivity.class);

                startActivity(contentintent);
            }
        });
    }
}